@extends('../layouts.base')
@section('css')
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link href='/css/coinbase.css' rel='stylesheet' type='text/css'>
@endsection
@section('content')
    <div class="container priceContainer">

        <h2>Account {{$data['idProduct']}}</h2>

        <table class="" id="portfolioTable" style="font-size: 18px;">
            <tr class="header">
                <td><b>Rendement</b></td>
                <td align="right">
                    <b>{{ number_format((($data['account_value'] - $data['trade']['bought'] - $data['trade']['sold']) * 100) / ($data['trade']['bought'] - $data['trade']['sold']) ,2)  }}</b>
                    %
                </td>
            </tr>
            <tr>
            <tr>
                <td>Resultaat:</td>
                <td align="right">{{number_format(($data['account_value'] - ($data['trade']['bought'] - $data['trade']['sold'])) ,2)}}
                    €
                </td>
            </tr>
            <tr>
                <td>Waarde portfolio:</td>
                <td align="right">{{ number_format($data['account_value'],2) }} €</td>
            </tr>
            <tr>
                <td>Ingelegd:</td>
                <td align="right">{{ number_format($data['trade']['bought'] - $data['trade']['sold'],2) }} €</td>
            </tr>
        </table>
        <br/>
        <br/>
        <table id="coinTable">
            <tr class="header">
                <th>Side</th>
                <th>Type</th>
                <th>Status</th>
                <th style="text-align: right">Waarde €</th>
                <th style="text-align: right"># coins</th>
                <th style="text-align: right">price €</th>
                <th style="text-align: right">Huidige prijs €</th>
                <th style="text-align: right">Result €</th>
                <th style="text-align: right">%</th>
                <th>Created</th>
            </tr>
            @foreach($data['orders'] as $order)
                <tr>
                    <td>{{$order->side}}</td>
                    <td>{{$order->type}}</td>
                    <td>{{$order->status }}</td>
                    <td style="text-align: right">{{ ($order->executed_value > 0 ? number_format($order->executed_value,2) : number_format($order->size * $order->price,2) )}}</td>
                    <td style="text-align: right">{{ ($order->filled_size > 0 ? $order->filled_size : $order->size ) }}</td>
                    <td style="text-align: right">{{ ($order->filled_size > 0 ? number_format($order->executed_value / $order->filled_size,3) : number_format( $order->price,3))}}</td>
                    <td style="text-align: right">{{ number_format( $order->value,3)}}</td>
                    <td style="text-align: right">{{ ($order->filled_size > 0 ? number_format( ($order->value * $order->filled_size) - $order->executed_value ,3) :'')}}</td>
                    <td style="text-align: right">{{ ($order->filled_size > 0 ? number_format( (($order->value * $order->filled_size) - $order->executed_value) *100 / $order->executed_value ,2) :'')}}</td>
                    <td>{{$order->created_at}}</td>
                </tr>
            @endforeach
        </table>

    </div>
@endsection
@section('js')
    <script src="/js/crypto.js"></script>
@endsection


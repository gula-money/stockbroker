@extends('../layouts.base')
@section('css')
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link href='/css/coinbase.css' rel='stylesheet' type='text/css'>
@endsection
@section('content')
    <div class="container priceContainer">

        <h2>Trade Orders</h2>
        <br/>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="graphs-tab" data-toggle="tab" href="#open" role="tab"
                   aria-controls="graphs" aria-selected="true"><img src="https://cms.gula.nl/resizer/36x36/cms/icons/lock_open.png" class="symbolRow"/>
                    Open Orders</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="alerts-tab" data-toggle="tab" href="#filled" role="tab" aria-controls="alerts"
                   aria-selected="false"><img src="https://cms.gula.nl/resizer/36x36/cms/icons/lock_closed.png" class="symbolRow"/> Filled Orders</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="alerts-tab" data-toggle="tab" href="#cancelled" role="tab" aria-controls="alerts"
                   aria-selected="false"><img src="https://cms.gula.nl/resizer/36x36/cms/icons/cancel.png" class="symbolRow"/> Cancelled Orders</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="open" role="tabpanel" aria-labelledby="graphs-tab">
                <br/>
                <table class="standard" id="coinTable">
                    <tr class="header">
                        <th>Product</th>
                        <th>Side</th>
                        <th>Type</th>
                        <th>TIF</th>
                        <th style="text-align: right">Amount</th>
                        <th style="text-align: right">Price/pc</th>
                        <th style="text-align: right">Current price</th>

                        <th>Created</th>
                        <th></th>
                    </tr>
                    @foreach($orders as $order)
                        @if($order->status == 'open' && !$order->cancelled)
                            <tr {{($order->cancelled ? 'class=cancelled': '')}}>
                                <td><a href="/coinbase/account/{{strtolower($order->product_id)}}" class="btn btn-warning btn active btn-block"
                                       role="button" aria-pressed="true">{{$order->product_id}}</a></td>
                                <td>{{$order->side}}</td>
                                <td>{{$order->type}}</td>
                                <td>{{($order->type == 'limit' ? $order->time_in_force : '')}}</td>
                                <td style="text-align: right">{{number_format($order->size,3)}}</td>
                                <td style="text-align: right">{{number_format($order->price,4)}}</td>
                                <td style="text-align: right">{{number_format($order->value,4)}}</td>
                                <td>{{$order->created_at}}</td>
                                <td>
                                    @if(!$order->cancelled)
                                        <img src="https://cms.gula.nl/resizer/36x36/cms/icons/cancel.png" class="symbolRow"
                                             onclick="cancelOrder('{{$order->id}}');false" title="Cancel Order"/>
                                        <img src="https://cms.gula.nl/resizer/36x36/cms/icons/view.png" class="symbolRow"
                                             onclick="viewOrder('{{$order->id}}');false" title="View Order"/>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
            <div class="tab-pane fade" id="filled" role="tabpanel" aria-labelledby="profile-orders">
                <br/>
                <table id="coinTable">
                    <tr class="header">
                        <th>Product</th>
                        <th>Side</th>
                        <th>Type</th>
                        <th>TIF</th>
                        <th style="text-align: right"># coins</th>
                        <th style="text-align: right">Paid €</th>
                        <th style="text-align: right">Coin price €</th>
                        <th>Created</th>
                        <th></th>
                    </tr>
                    @foreach($orders as $order)
                        @if($order->status !== 'open')
                            <tr>
                                <td>
                                    <a href="/coinbase/product/{{strtolower($order->product_id)}}">{{$order->product_id}}</a>
                                </td>
                                <td>{{$order->side}}</td>
                                <td>{{$order->type}}</td>
                                <td>{{($order->type == 'limit' ? $order->time_in_force : '')}}</td>
                                <td style="text-align: right">{{$order->filled_size}}</td>
                                <td style="text-align: right">{{number_format($order->executed_value,8)}}</td>
                                <td style="text-align: right">{{number_format($order->funds / $order->filled_size,8)}}</td>
                                <td>{{$order->created_at}}</td>
                                <td>
                                    <img src="https://cms.gula.nl/resizer/36x36/cms/icons/view.png" class="symbolRow"
                                         onclick="viewOrder('{{$order->id}}');false" title="View Order"/>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>
            <div class="tab-pane fade show" id="cancelled" role="tabpanel" aria-labelledby="graphs-tab">
                <br/>
                <table class="standard" id="coinTable">
                    <tr class="header">
                        <th>Product</th>
                        <th>Side</th>
                        <th>Type</th>
                        <th>TIF</th>
                        <th style="text-align: right">Amount</th>
                        <th style="text-align: right">Price/pc</th>
                        <th>Created</th>
                        <th></th>
                    </tr>
                    @foreach($orders as $order)
                        @if($order->status == 'open' && $order->cancelled)
                            <tr {{($order->cancelled ? 'class=cancelled': '')}}>
                                <td>
                                    <a href="/coinbase/product/{{strtolower($order->product_id)}}">{{$order->product_id}}</a>
                                </td>
                                <td>{{$order->side}}</td>
                                <td>{{$order->type}}</td>
                                <td>{{($order->type == 'limit' ? $order->time_in_force : '')}}</td>
                                <td style="text-align: right">{{number_format($order->size,3)}}</td>
                                <td style="text-align: right">{{number_format($order->price,4)}}</td>
                                <td>{{$order->created_at}}</td>
                                <td>
                                    @if(!$order->cancelled)
                                        <img src="https://cms.gula.nl/resizer/36x36/cms/icons/cancel.png" class="symbolRow"
                                             onclick="cancelOrder('{{$order->id}}');false" title="Cancel Order"/>
                                        <img src="https://cms.gula.nl/resizer/36x36/cms/icons/view.png" class="symbolRow"
                                             onclick="viewOrder('{{$order->id}}');false" title="View Order"/>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </table>
            </div>

        </div>
        <br/>
        <br/>
        <br/>
        <br/>
    </div>
@endsection
@section('js')
    <script src="/js/crypto.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" crossorigin="anonymous"></script>

@endsection



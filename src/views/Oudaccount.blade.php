@extends('../layouts.base')
@section('css')
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link href='/css/coinbase.css' rel='stylesheet' type='text/css'>
@endsection
@section('content')
    <div class="container priceContainer">
        {{csrf_field()}}
        <h2>{{strtoupper($data['id_product'])}}</h2>
        <br/>
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="graphs-tab" data-toggle="tab" href="#graphs" role="tab"
                   aria-controls="graphs" aria-selected="true"><img src="https://cms.gula.nl/resizer/36x36/cms/icons/graph.png" class="symbolRow"/> Graphs</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="orders-tab" data-toggle="tab" href="#orders" role="tab" aria-controls="orders"
                   aria-selected="false"><img src="https://cms.gula.nl/resizer/36x36/cms/icons/trade.png" class="symbolRow"/> Orders</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="alerts-tab" data-toggle="tab" href="#alerts" role="tab" aria-controls="alerts"
                   aria-selected="false"><img src="https://cms.gula.nl/resizer/36x36/cms/icons/alert.png" class="symbolRow"/> Alert Rules</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="stats-tab" data-toggle="tab" href="#stats" role="tab" aria-controls="stats"
                   aria-selected="false"><img src="https://cms.gula.nl/resizer/36x36/cms/icons/alert.png" class="symbolRow"/> Stats Alerts</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="graphs" role="tabpanel" aria-labelledby="graphs-tab">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <div style="width: 800px;height: 300px;">
                                <canvas id="coinChart1week" width="450" height="300"></canvas>
                            </div>
                            <br/>
                        </div>
                        <div class="row">
                            <div style="width: 800px;height: 300px;margin-top: 250px;">
                                <canvas id="coinChart1month" width="450" height="300"></canvas>
                            </div>
                        </div>
                        <div class="row">
                            <div style="width: 800px;height: 300px;margin-top: 250px;">
                                <canvas id="coinChart1quarter" width="450" height="300"></canvas>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-5 text-right">
                                <br/>
                                Huidige koers:<br/><br/>
                                -1 uur :<br/><br/>
                                -1 dag :<br/><br/>
                                -1 week :<br/><br/>
                                -1 maand :<br/><br/>
                                -3 maanden :<br/><br/>
                            </div>
                            <div class="col-md-5 text-right">
                                <br/>
                                {{number_format($data['current_prices'][strtoupper($data['id_product'])]->price,8)}}<br/><br/>
                                {{number_format($data['current_prices'][strtoupper($data['id_product'])]->price_1hour,8)}}<br/><br/>
                                {{number_format($data['current_prices'][strtoupper($data['id_product'])]->price_1day,8)}}<br/><br/>
                                {{number_format($data['current_prices'][strtoupper($data['id_product'])]->price_1week,8)}}<br/><br/>
                                {{number_format($data['current_prices'][strtoupper($data['id_product'])]->price_1month,8)}}<br/><br/>
                                {{number_format($data['current_prices'][strtoupper($data['id_product'])]->price_1quarter,8)}}<br/><br/>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <div class="row">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#buyModal">
                                Buy Order
                            </button>
                            <div style="margin-left: 40px;"></div>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#sellModal">
                                Sell Order
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="orders" role="tabpanel" aria-labelledby="profile-orders">...</div>
            <div class="tab-pane fade" id="alerts" role="tabpanel" aria-labelledby="alerts-tab">...</div>
            <div class="tab-pane fade" id="stats" role="tabpanel" aria-labelledby="contact-stats">starts alert</div>
        </div>
        <br/>
        <br/>
        <br/>
        <br/>

    </div>
    <div class="modal fade" id="buyModal" tabindex="-1" role="dialog" aria-labelledby="buyModal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ebuySellModallLabel">{{strtoupper($data['id_product']) . ' BUY '}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden"  id="side" name="side" value="buy" />
                    <div class="form-group">
                        <label for="product_id">Coin</label>
                        <select class="form-control" id="product_id" name="product_id">
                            @foreach($data['current_prices'] as $product)
                                <option value="{{$product->id_product}}" @if($product->id_product == strtoupper($data['id_product']))selected @endif>{{$product->id_product}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="side">Type</label>
                        <select class="form-control" id="type" name="type">
                            <option value="market">Market</option>
                            <option value="limit">Limit</option>
                            <option value="stop">Stop ?</option>
                        </select>
                    </div>
                    <div class="form-group size" style="display: none">
                        <label for="size">Size lijkt aantal btc coins </label>
                        <input type="number" class="form-control" id="size" name="size" value="0">
                    </div>
                    <div class="form-group funds">
                        <label for="size">Funds (max {{$data['wallets'][strtoupper(substr($data['id_product'],-3))] . ' ' . strtoupper(substr($data['id_product'],-3))}})</label>
                        <input type="number" class="form-control" id="funds" name="funds" value="0" max="{{$data['wallets'][strtoupper(substr($data['id_product'],-3))]}}">
                    </div>
                    <div class="form-group price" style="display: none">
                        <label for="price">Price (max price {{strtoupper($data['id_product'])}})</label>
                        <input type="number" class="form-control" id="price" name="price" value="0">
                    </div>
                    <div class="form-group time_in_force" style="display: none">
                        <label for="side">Time In Force Policy</label>
                        <select class="form-control" id="time_in_force" name="time_in_force">
                            <option value="GTC">Good Till Cancelled</option>
                            <option value="GIT">Good Till Time</option>
                            <option value="IOC">Immediate or Cancel</option>
                            <option value="FOK">Fill or Kill</option>
                        </select>
                    </div>
                    <div class="form-group cancel_after" style="display: none">
                        <label for="price">Cancel after</label>
                        <select class="form-control" id="cancel_after" name="cancel_after">
                            <option value="min">One Minute</option>
                            <option value="hour">One Hour</option>
                            <option value="day">One Day</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" id="execute_buy_order" class="btn btn-primary">PLACE BUY ORDER</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script src="/js/crypto.js"></script>
    <script>
        document.getElementById('type').addEventListener("change", changeType);
        document.getElementById('time_in_force').addEventListener("change", changeTIF);
        document.getElementById('execute_buy_order').addEventListener("click", validateBuyOrder);

    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js" crossorigin="anonymous"></script>
    <script>
        drawGraph([{{$data['1w']['dataString']}}], [{!! $data['1w']['labelString'] !!}], 'coinChart1week', 'last week');
        drawGraph([{{$data['1m']['dataString']}}], [{!! $data['1m']['labelString'] !!}], 'coinChart1month', 'last month');
        drawGraph([{{$data['1q']['dataString']}}], [{!! $data['1q']['labelString'] !!}], 'coinChart1quarter', 'last quarter');
    </script>

@endsection



@extends('../layouts.base')
@section('css')
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link href='/css/coinbase.css' rel='stylesheet' type='text/css'>
@endsection
@section('content')
    <div class="container priceContainer">

        <h2>Scenario's</h2>
        <a href="/coinbase/scenario/new">
            <button class="btn btn-success">Create new scenario</button>
        </a>
        <br/>
        <br/>
        <input type="text" id="coinSearch" onkeyup="coinFilter()" placeholder="Search for crypto scenario's.."
               title="Type in a coin name">

        <table id="coinTable">
            <tr class="header">
                <th style="width:3%;">id</th>
                <th style="width:12%;">coin</th>
                <th style="width:15%;">name</th>
                <th style="width:5%;text-align: left">side</th>
                <th style="width:7%;text-align: right">size</th>
                <th style="width:10%;text-align: right">price €</th>
                <th style="width:5%;text-align: right">link</th>
                <th style="width:17%;text-align: left">executed</th>
                <th style="width:17%;text-align: left">uploaded</th>
            </tr>
            @foreach($data['scenarios'] as $scenario)

                    <tr>
                        <td align="right"><a href="/coinbase/scenario/{{$scenario->id}}/edit" style="text-decoration: none; color: dimgrey">{{$scenario->id}}</a></td>
                        <td>{{$scenario->product_id}}</td>
                        <td>{{$scenario->name}}</td>
                        <td>{{$scenario->side}}</td>
                        <td align="right">{{number_format($scenario->size, 4)}}</td>
                        <td align="right">{{number_format($scenario->price,4)}}</td>
                        <td align="right">{{$scenario->depending_on_scenario}}</td>
                        <td align="left">{{$scenario->executed_at}}</td>
                        <td align="left">{{$scenario->uploaded_at}}</td>

                    </tr>

            @endforeach
        </table>
        <br/>
        <br/>
        <br/>
        <br/>
    </div>
@endsection
@section('js')
    <script src="/js/crypto.js"></script>
@endsection


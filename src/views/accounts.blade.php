@extends('../layouts.base')
@section('css')
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link href='/css/coinbase.css' rel='stylesheet' type='text/css'>
@endsection
@section('content')
    <div class="container priceContainer">

        <h2>Portfolio</h2>

        <table class="" id="portfolioTable" style="font-size: 18px;">
            <tr class="header">
                <td><b>Rendement</b></td>
                <td align="right"><b>{{   number_format((($data['portfolio_value'] - ($data['transfers']['deposit'] - $data['transfers']['withdraw'])) * 100) / ($data['transfers']['deposit'] - $data['transfers']['withdraw']) ,2)  }}</b> %</td>
            </tr>
            <tr>
            <tr>
                <td>Resultaat:</td>
                <td align="right">{{number_format(($data['portfolio_value'] - ($data['transfers']['deposit'] - $data['transfers']['withdraw'])) ,2)}} €</td>
            </tr>
            <tr>
                <td>Waarde portfolio:</td>
                <td align="right">{{ number_format($data['portfolio_value'],2) }} €</td>
            </tr>
            <tr>
                <td>Ingelegd:</td>
                <td align="right">{{ number_format($data['transfers']['deposit'] - $data['transfers']['withdraw'],2) }} €</td>
            </tr>
        </table>
        <br/>
        <br/>

        <input type="text" id="coinSearch" onkeyup="coinFilter()" placeholder="Search for crypto coins.."
               title="Type in a coin name">

        <table id="coinTable">
            <tr class="header">
                <th style="width:10%;">Account</th>
                <th style="width:15%;text-align: right">Rendement</th>
                <th style="width:15%;text-align: right">Resultaat €</th>
                <th style="width:15%;text-align: right">Waarde €</th>
                <th style="width:15%;text-align: right">Ingelegd €</th>
                <th style="width:15%;text-align: right"># coins</th>
                <th style="width:15%;text-align: right">Huidige koers €</th>

            </tr>
            @foreach($data['accounts'] as $account)
                <tr>
                    <td><a href="/coinbase/account/{{strtolower($account->currency)}}" class="btn btn-warning btn active btn-block"
                           role="button" aria-pressed="true">{{$account->currency}}</a></td>
                    <td align="right"><b>{{ (($account->bought - $account->sold) <> 0 ? number_format(((($account->balance * $account->value) - ($account->bought - $account->sold)) *100) / ($account->bought) , 2) .' %' : '')}}</b></td>
                    <td align="right">{{ (($account->bought - $account->sold) <> 0 ? number_format((($account->balance * $account->value) - ($account->bought - $account->sold)) , 2) : '')}}</td>
                    <td align="right">{{((!empty($account->value) && $account->balance > 0) ? number_format($account->balance * $account->value, 2) : '')}}</td>
                    <td align="right">{{ ($account->bought > 0 ? number_format($account->bought - $account->sold, 2) : '')}}</td>
                    <td align="right">{{($account->balance <> 0 ? number_format($account->balance,6) : '')}}</td>
                    <td align="right">{{!empty($account->value) ? number_format($account->value, 6) :''}}</td>

                </tr>
            @endforeach
        </table>
        <br/>
        <br/>
        <br/>
        <br/>
    </div>
@endsection
@section('js')
    <script src="/js/crypto.js"></script>
@endsection


@extends('../layouts.base')
@section('css')
    <link href='https://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
    <link href='/css/coinbase.css' rel='stylesheet' type='text/css'>
@endsection
@section('content')
    <div class="container priceContainer">

        <h2>Scenario {{$data['scenario']->id}} {{$data['scenario']->name}}</h2>
        <br/>
        <div class="card-body">
            <form method="post" action="/coinbase/scenario/store" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="id" value="{{$data['scenario']->id}}">
                <div class="form-group">
                    <label for="name">Beschrijving:</label>
                    <input type="text" class="form-control" name="name" value="{{$data['scenario']->name}}">
                </div>
                <div class="form-group">
                    <label for="depending_on_scenario">Afhankelijk van scenario:</label>
                    <select class="form-control" id="depending_on_scenario" name="depending_on_scenario">
                        <option></option>
                        @foreach($data['scenarios'] as $scenario)
                            <option value="{{$scenario->id}}" @if($data['scenario']->depending_on_scenario == $scenario->id) selected @endif>{{$scenario->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="product_id">Crypto munt:</label>
                    <select class="form-control" id="product_id" name="product_id">
                        <option>Kies een crypto coin</option>
                        @foreach($data['accounts'] as $account)
                            <option value="{{$account->currency}}-EUR" @if($data['scenario']->product_id === $account->currency . '-EUR') selected @endif>{{$account->currency}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="side">Actie:</label>
                    <select class="form-control" id="side" name="side">
                        <option value="buy" @if($data['scenario']->side === 'buy') selected @endif>Kopen</option>
                        <option value="sell" @if($data['scenario']->side === 'sell') selected @endif>Verkopen</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="price">Prijs €</label>
                    <input type="number" class="form-control" name="price" min="0" step="0.0001" value="{{$data['scenario']->price}}">
                </div>

                <div class="form-group">
                    <label for="price">Huidige prijs €</label>
                    <input type="number" class="form-control" id="current_price" value="" disabled>
                </div>

                <div class="form-group">
                    <label for="size">Aantal (munten) &nbsp;&nbsp;&nbsp;<a class="" href="#calculate_euro" data-toggle="modal" data-target="#convertEuro"><img src="https://cms.gula.nl/resizer/14x14/cms/icons/euro.png" /> Euro's omrekenen</a>
                    </label>
                    <input type="number" class="form-control" min="0" step="0.0000001" name="size" value="{{$data['scenario']->size}}">
                </div>

                <div class="form-group">
                    <label for="price">Order geplaatst op:</label>
                    <input type="text" class="form-control" name="uploaded_at" id="uploaded_at" value="{{$data['scenario']->uploaded_at}}" disabled>
                </div>

                <div class="form-group">
                    <label for="price">Order uigevoerd op:</label>
                    <input type="text" class="form-control" name="executed_at" id="executed_at" value="{{$data['scenario']->executed_at}}" disabled>
                </div>

                <div class="form-group form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="deleted"
                               {{ ($data['scenario']->deleted ? 'checked':'') }} !!} > Uitgeschakeld
                    </label>
                </div>
                <button type="submit" class="btn btn-success">Sla scenario op</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                <a href="/coinbase/scenario/{{$data['scenario']->id}}/delete" ><button type="button" class="btn btn-danger">Trek scenario in</button></a>
            </form>
        </div>
        <br/>
        <br/>
    </div>

    <div class="modal fade" id="convertEuro" tabindex="-1" role="dialog" aria-labelledby="convertEuro" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="convertEuroLabel">Kies eerst een coin</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="convertEuroCurrentPrice"></p>
                    <label for="price">Bedrag in euro:</label>
                    <input type="number" class="form-control col-4" min="0" step="0.0000001" id="convertEuro" value="0">
                    <br/>
                    <label for="price"># coins:</label>
                    <input type="number" class="form-control col-4" disabled>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-cancel" data-dismiss="modal">Sluiten</button>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="/js/crypto.js"></script>

@endsection


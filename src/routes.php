<?php

Route::group(['middleware' => 'web', 'namespace' => 'Gula\Stockbroker\Controllers'], function()
{
    Route::get('/coinbase/update_coin_prices','CoinbaseController@updateCoinPrices')->name('updateCoinbaseCoinPrices');
    Route::get('/coinbase/update_products','CoinbaseController@updateProducts')->name('updateCoinbaseProducts');
    Route::get('/coinbase/update_accounts','CoinbaseController@updateAccounts')->name('updateCoinbaseAccounts');
    Route::get('/coinbase/update_orders','CoinbaseController@updateOrders');
    Route::get('/coinbase/update_payment_methods','CoinbaseController@updatePaymentMethods')->name('updateCoinbasePaymentMethods');
    Route::get('/coinbase/update_products_prices','CoinbaseController@updateProductsPrices')->name('updateCoinbaseProductsPrices');
    Route::get('/coinbase/update_historical_prices','CoinbaseController@updateHistoricalPrices')->name('updateCoinbaseHistoricalPrices');
    Route::get('/coinbase/update_transfers','CoinbaseController@updateTransfers');
    Route::get('/coinbase/orders','CoinbaseController@orders')->name('getCoinbaseOrders');
    Route::get('/coinbase/accounts','CoinbaseController@accounts')->name('coinbaseAccounts');
    Route::get('/coinbase/account/{idProduct}','CoinbaseController@viewAccount')->name('coinbaseviewAccount');
    Route::get('/coinbase/auto_trade/{strategy}','CoinbaseController@autoTrade')->name('coinbaseAutoTrade');
    Route::get('/coinbase/analysis','CoinbaseController@analysis')->name('coinbaseAnalysis');
    Route::get('/coinbase/scenarios','CoinbaseController@scenarios')->name('coinbaseScenarios');
    Route::get('/coinbase/scenario/new','CoinbaseController@newScenario');
    Route::get('/coinbase/scenario/{idScenario}/delete','CoinbaseController@deleteScenario');
    Route::get('/coinbase/scenario/{idScenario}/edit','CoinbaseController@editScenario');
    Route::post('/coinbase/scenario/store','CoinbaseController@storeScenario');
    Route::get('/coinbase/scenarios/run','CoinbaseController@runScenarios');
    Route::get('/coinbase/get_price','CoinbaseController@getPrice');
});



Route::get('test', function(){
    echo 'Hello from the stockbroker package!';
});

<?php

namespace Gula\Stockbroker\Controllers;

use Gula\Stockbroker\Controllers\Library\Analysis;
use Gula\Stockbroker\Models\Coinbase\Accounts;
use Gula\Stockbroker\Models\Coinbase\Client;
use Gula\Stockbroker\Models\Coinbase\Logs;
use Gula\Stockbroker\Models\Coinbase\Orders;
use Gula\Stockbroker\Models\Coinbase\PaymentMethods;
use Gula\Stockbroker\Models\Coinbase\Products;
use Gula\Stockbroker\Models\Coinbase\ProductsHistoricalPrices;
use Gula\Stockbroker\Models\Coinbase\ProductsPrices;
use Gula\Stockbroker\Models\Coinbase\Scenarios;
use Gula\Stockbroker\Models\Coinbase\Transfers;
use Illuminate\Http\Request;
use PharIo\Manifest\Library;


class CoinbaseController extends AbstractController
{
    public function updateCoinPrices(Request $request)
    {
        die('update_coin_prices');
    }

    public function updateProducts()
    {
        $coinbase = new Client();
        $mdlProducts = new Products();
        $products = json_decode($coinbase->doRequest('/products', null, 'GET', true));

        $mdlProducts->update($products);
    }

    public function updateAccounts()
    {
        $coinbase = new Client();
        $mdlAccounts = new Accounts();
        $accounts = json_decode($coinbase->doRequest('/accounts', null, 'GET', false));

        $mdlAccounts->update($accounts);
    }

    public function updatePaymentMethods()
    {
        $coinbase = new Client();
        $mdlPaymentMethods = new PaymentMethods();
        $methods = json_decode($coinbase->doRequest('/payment-methods', null, 'GET', false));

        foreach ($methods as $key => $item) {
            unset($methods[$key]->created_at);
            unset($methods[$key]->updated_at);
            unset($methods[$key]->resource);
            unset($methods[$key]->limits);
            unset($methods[$key]->verified);
            unset($methods[$key]->picker_data);
            unset($methods[$key]->hold_business_days);
            unset($methods[$key]->hold_days);
            unset($methods[$key]->fiat_account);
            unset($methods[$key]->instant_buy);
            unset($methods[$key]->instant_sell);
            unset($methods[$key]->resource_path);
            unset($methods[$key]->minimum_purchase_amount);
        }

        $mdlPaymentMethods->update($methods);
    }

    public function updateProductsPrices()
    {
        $coinbase = new Client();
        $mdlProducts = new Products();
        $mdlProductsPrices = new ProductsPrices();
        $log = new Logs();

        foreach ($mdlProducts->getProducts(false, true) as $product) {
            $price = json_decode($coinbase->doRequest('/products/' . $product->id . '/ticker', null, 'GET', false));

            if(isset($price->price)){
                $mdlProducts->updatePrice($product->id, $price->price);
            }
        }

//        $log->store('products', 'Prices updated');
    }

    public function updateHistoricalPrices()
    {
        $coinbase = new Client();
        $mdlProducts = new Products();
        $mdlProductsHistoricalPrices = new ProductsHistoricalPrices();

        $mondays = $this->getWeeksScope();
        $year = date('Y');
        $wk = date("W");

        foreach ($mdlProducts->getProducts(false) as $product) {
            foreach ($mondays as $monday) {

                if (false === $mdlProductsHistoricalPrices->historyExists($product->id, $monday) || $monday == date("Y-m-d", strtotime("{$year}-W{$wk}-1"))) {
                    $date = strtotime($monday);
                    $endDate = date('Y-m-d', strtotime('+7 days', $date));

                    $rates = json_decode($coinbase->doRequest(('/products/' . $product->id . '/candles?start=' . $monday . '&end=' . $endDate . '&granularity=3600'), null, 'GET'));

                    if (is_array($rates) && count($rates) > 0) {
                        $mdlProductsHistoricalPrices->updateRates($product->id, $rates, $monday);
                    }
                }
            }
        }
    }

    public function orders(Request $request)
    {
        $mdlOrders = new Orders();

        $orders = $mdlOrders->getOrders();

        return view('stockbroker::orders', compact('orders'));
    }

    public function updateTransfers()
    {
        $coinbase = new Client();
        $mdlTransfers = new Transfers();

        $transfers = $price = json_decode($coinbase->doRequest('/transfers/', null, 'GET', false));
        $mdlTransfers->updateTransfer($transfers);

        return view('home');
    }

    public function updateOrders()
    {
        $update = (new Orders())->updateOrders();

        return view('home');
    }

    public function accounts()
    {
        $this->updateAccounts();

        $data = [
            'transfers' => (new Transfers())->getTotals(),
            'accounts' => (new Accounts())->getAccounts(true),
            'portfolio_value' => (new Accounts())->getPortfolioValue(),
        ];

        return view('stockbroker::accounts', compact('data'));
    }

    public function viewAccount($idProduct)
    {
        $idProduct = str_replace('-eur', '', $idProduct);

        $coinbase = new Client();
        $mdlAccounts = new Accounts();

        $data = [
            'account' => (new Accounts())->getAccount($idProduct),
            'orders' => (new Orders())->getAccountOrders($idProduct),
            'idProduct' => strtoupper($idProduct),
            'account_value' => (new Accounts())->getAccountValue($idProduct),
            'trade' => (new Orders())->getTrading($idProduct),
        ];

        return view('stockbroker::account', compact('data'));
    }

    public function analysis()
    {
        $analysis = new Gula\Stockbroker\Library\Analysis();
        $analysis->start(date('Y-m-d', strtotime('-3 months')));
    }

    public function autoTrade($strategy)
    {

        $strategy = 'Gula\Stockbroker\Controllers\Library\Trade' . $this->getCamelCase($strategy);
        $tradeController = new $strategy();

        $tradeController->analyze();


    }

    public function getStats(string $id_product)
    {
        $coinbase = new Client();

        return $coinbase->doRequest('/products/' . $id_product . '/stats', null, 'GET');
    }

    private function getCamelCase(string $str): string
    {
        $strArray = explode('_', $str);
        foreach ($strArray as &$item) {
            $item = ucfirst($item);
        }

        return implode("", $strArray);
    }

    public function getPricesSpecificPeriod(string $idProduct, $startDate, $endDate)
    {
        $coinbase = new Client();

        return json_decode($coinbase->doRequest(('/products/' . $idProduct . '/candles?start=' . $startDate . '&end=' . $endDate . '&granularity=300'), null, 'GET'));
    }

    public function scenarios()
    {
        $data = [
            'scenarios' => (new Scenarios())->list()
        ];

        return view('stockbroker::scenarios', compact('data'));
    }

    public function newScenario()
    {
        $idScenario = (new Scenarios())->getIdCreated();

        return redirect('/coinbase/scenario/' . $idScenario . '/edit');
    }

    public function editScenario($idScenario)
    {
        $mdlScenarions = new Scenarios();

        $data = [
            'scenario' => $mdlScenarions->get($idScenario),
            'scenarios' => $mdlScenarions->list(),
            'accounts' => (new Accounts())->getAccounts(),
        ];
        return view('stockbroker::scenario', compact('data'));
    }

    public function storeScenario(Request $request)
    {
        $result = (new Scenarios())->store($request->post());

        return redirect('/coinbase/scenarios');
    }

    public function deleteScenario($idScenario)
    {
        $result = (new Scenarios())->disable($idScenario);

        return redirect('/coinbase/scenarios');

    }

    public function runScenarios()
    {
        $scenarios = new \Gula\Stockbroker\Controllers\Library\Scenarios();
        $scenarios->parse();
    }

    public function getPrice(Request $request)
    {
        return (new Products())->getCurrentPrice($name = $request->input('product_id'));
        $result = ['value' => 'test'];
//        $result = ['value' => (new Products())->getCurrentPrice($name = $request->input('product_id'))];

        return json_encode($result);
    }
}

<?php


namespace Gula\Stockbroker\Controllers;


use App\Http\Controllers\Controller;

class AbstractController extends Controller
{
    public function getWeeksScope(): array
    {
        $previousWeek = date("W");

        //previous year
        $year = date('Y')-1;
        for($week = $previousWeek -1;$week < 53; $week++){
            $wk = substr('0'. $week, -2);
            $scope[] = date("Y-m-d", strtotime("{$year}-W{$wk}-1"));
        }

        //current year
        $year = date('Y');
        for($week = 1;$week <= $previousWeek; $week++){
            $wk = substr('0'. $week, -2);
            $scope[] = date("Y-m-d", strtotime("{$year}-W{$wk}-1"));
        }

        return $scope;
    }

}

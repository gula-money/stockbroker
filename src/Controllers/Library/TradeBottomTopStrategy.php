<?php


namespace Gula\Stockbroker\Controllers\Library;


use Gula\Stockbroker\Controllers\CoinbaseController;
use Gula\Stockbroker\Models\Coinbase\Logs;
use Gula\Stockbroker\Models\Coinbase\Strategies;
use Gula\Stockbroker\Models\Coinbase\StrategyQueue;

/**
 * Class TradeBottomTopStrategy
 * @package Gula\Stockbroker\Controllers\Library
 * Predict bottom -> buy -> Predict top ->sell
 */
class TradeBottomTopStrategy extends CoinbaseController
{
    protected $lookBackPeriod = '1 day';
    protected $depreciationPercentage = -10;
    protected $increasementPercentage = 10;
    protected $stategyProducts = [];
    protected $strategyName = 'bottom_top_strategy';
    protected $maxFunds = 500;

    public function analyze()
    {
        $this->stategyProducts = (new Strategies())->getTradebleProducts($this->strategyName);

        $log = new Logs();
        $queue = new StrategyQueue();
//        $log->store($this->strategyName, 'start analyzing ');

        $startDate = date("Y-m-d\TH:i", strtotime('-1 days'));
        $endDate = date("Y-m-d\TH:i", strtotime('+1 hour'));

        foreach ($this->stategyProducts as $stategyProduct) {

//            $stategyProduct->id_product = 'BCH-EUR'; //@todo weghalen

            $rates = $this->getPricesSpecificPeriod($stategyProduct->id_product, $startDate, $endDate);

            if(true === $this->wasDeclining($rates, 48, -2.00) && true === $this->isRising($rates, 6, 1.00) ){

                $queue->store([
                    'product_id' => $stategyProduct->id_product,
                    'buy_size' => (100/$rates[0][4]),
                    'buy_price' => $rates[0][4],
                    'buy_executed_at' => now(),
                    'sell_size' => (100/$rates[0][4]),
                    'sell_price' => ($rates[0][4] * 1.03)
                ]);

                $log->store($this->strategyName, 'buy: ' . $stategyProduct->id_product . ': ' . $rates[0][4]);

                //            if(true === $this->wasDeclining($rates, 48, -1.00)  ){
                // kopen
                echo 'buy: ' . $stategyProduct->id_product . ': ' . $rates[0][4] . '<br>';
                $a=1;
            }

            usleep (200000);
            //todo hoogtepunt van stijging bepalen. Alleen indien er een aankoop is geweest



            //dieptepunt geweest?
            //kopen
//laatste 6 uur laagste punt
//stijging in laatste uur

        }
        //sell
        //buy
//        $this->placeOrders();

        echo 'klaar';
    }

    /**
     * @param array $rates
     * @param int $period   //
     * @param float|int $percentage
     * @return bool
     */
    protected function isRising(array $rates, int $period, float $percentage = 3): bool
    {
        $rising = true;
        $currentPrice = $rates[0][4];

        for($p = 1; $p < $period; $p++){
            if($rates[$p][4] > $currentPrice){
                $rising = false;
            }
        }

        return $rising;
    }

    /**
     * @param array $rates
     * @param int $period   //1 period = 5 minutes
     * @param float|int $percentage
     * @return bool
     */
    protected function wasDeclining(array $rates, int $period, float $percentage = -5): bool
    {
        $previousPrice = $rates[1][4];
        $bankingPrice = $rates[$period][4];

        return (($previousPrice - $bankingPrice) * 100) / $bankingPrice < $percentage ? true : false;
    }



}


<?php


namespace Gula\Stockbroker\Controllers\Library;

use Gula\Stockbroker\Controllers\CoinbaseController;
use Gula\Stockbroker\Models\Coinbase\Client;
use Gula\Stockbroker\Models\Coinbase\Logs;

class Scenarios extends CoinbaseController
{
    protected $model = 'scenarios';

    public function init()
    {

    }

    public function parse()
    {
        echo 'Begin:' . date("Y-m-d H:i:s") . '<br>';

        $this->updateProductsPrices();
        $this->updateOrders();
        $this->updateFromOrders();
        $this->parseScenarios();


        echo 'Einde:' . date("Y-m-d H:i:s");
//        $this->testMarketBuy();
        die('1');
//        $this->testCancel();
//        $this->testSell();
//        $this->testBuy();

    }

    public function updateFromOrders()
    {
        //@todo VULLEN OF ORDER IS UITGEVOERD
    }

    public function parseScenarios()
    {
        $log = new Logs();
        $mdlScenarios = new \Gula\Stockbroker\Models\Coinbase\Scenarios();
        $coinbase = new Client();
        $scenarios = (new \Gula\Stockbroker\Models\Coinbase\Scenarios())->getOpen();

//        $log->store($this->model, 'Start parsing scenario\'s');

        foreach ($scenarios as $scenario)
        {
            if($scenario->side === 'buy'){
                if(!$scenario->depending_on_scenario && $scenario->value <= $scenario->price){

                    $order = [
                        'type' => 'market',
                        'side' => 'buy',
                        'product_id' => $scenario->product_id,
                        'size' => $scenario->size
                    ];

                    $order = json_decode($coinbase->doRequest('/orders', $order, 'POST', true));

                    $mdlScenarios->updateScenario([
                        'id' => $scenario->id,
                        'id_order' => $order->id,
                        'executed_at' => date_format(date_create($order->created_at), 'Y-m-d H:i:s'),
                    ]);

                    $log->store($this->model, 'market buy, scenario ' .  $scenario->id . ', ' . $scenario->product_id . ', size: ' . $scenario ->size . ', price: ' . $scenario->price . ', idOrder: ' . $order->id);

                } elseif($scenario->depending_on_scenario && true === $this->isFullfilledDependency($scenario->depending_on_scenario)) {
                    //@todo size checken op ordewr
                    $order = [
                        'type' => 'limit',
                        'side' => 'buy',
                        'product_id' => $scenario->product_id,
                        'size' => $scenario->size,
                        'price' => $scenario->price
                    ];

                    $order = json_decode($coinbase->doRequest('/orders', $order, 'POST', true));

                    $mdlScenarios->updateScenario([
                        'id' => $scenario->id,
                        'id_order' => $order->id,
                        'uploaded_at' => date_format(date_create($order->created_at), 'Y-m-d H:i:s'),
                    ]);

                    $log->store($this->model, 'limit buy, scenario ' .  $scenario->id . ', ' . $scenario->product_id . ', size: ' . $scenario ->size . ', price: ' . $scenario->price . ', idOrder: ' . $order->id);
                }
            }

            if($scenario->side === 'sell'){
                if(!$scenario->depending_on_scenario && $scenario->value >= $scenario->price){
                    $order = [
                        'type' => 'market',
                        'side' => 'sell',
                        'product_id' => $scenario->product_id,
                        'size' => $scenario->size
                    ];

                    $order = json_decode($coinbase->doRequest('/orders', $order, 'POST', true));

                    $mdlScenarios->updateScenario([
                        'id' => $scenario->id,
                        'id_order' => $order->id,
                        'executed_at' => date_format(date_create($order->created_at), 'Y-m-d H:i:s'),
                    ]);

                    $log->store($this->model, 'market sell, scenario ' .  $scenario->id . ', ' . $scenario->product_id . ', size: ' . $scenario ->size . ', price: ' . $scenario->price . ', idOrder: ' . $order->id);

                } elseif($scenario->depending_on_scenario && true === $this->isFullfilledDependency($scenario->depending_on_scenario)) {
                    //@todo size checken op ordewr

                    $order = [
                        'type' => 'limit',
                        'side' => 'sell',
                        'product_id' => $scenario->product_id,
                        'size' => $scenario->size,
                        'price' => $scenario->price,
                    ];

                    $order = json_decode($coinbase->doRequest('/orders', $order, 'POST', true));

                    $mdlScenarios->updateScenario([
                        'id' => $scenario->id,
                        'id_order' => $order->id,
                        'uploaded_at' => date_format(date_create($order->created_at), 'Y-m-d H:i:s'),
                    ]);

                    $log->store($this->model, 'limit sell, scenario ' .  $scenario->id . ', ' . $scenario->product_id . ', size: ' . $scenario ->size . ', price: ' . $scenario->price . ', idOrder: ' . $order->id);
                }
            }
        }
    }

    public function isFullfilledDependency(int $idDependingScenario):bool
    {
        $dependingScenario = (new \Gula\Stockbroker\Models\Coinbase\Scenarios())->get($idDependingScenario);
        $idOrder = $dependingScenario->id_order;

        if($idOrder === null){
            return false;
        }

        $order = json_decode((new Client())->doRequest('/orders/' . $idOrder, null, 'GET', false));

        return $order->status === 'done' ? true : false;
    }

    public function testBuy()
    {
        //size = bedrag in euro gedeeld door price

        $order = [
            'type' => 'limit',
            'side' => 'buy',
            'product_id' => 'XLM-EUR',
            'price' => 0.28,
            'size' => 10
        ];

        $coinbase = new Client();
        $order = json_decode($coinbase->doRequest('/orders', $order, 'POST', true));

        $a=1;
    }

    public function testMarketBuy()
    {
        $order = [
            'type' => 'market',
            'side' => 'buy',
            'product_id' => 'XLM-EUR',
            'size' => 10
        ];

        $coinbase = new Client();
        $order = json_decode($coinbase->doRequest('/orders', $order, 'POST', true));

        $a=1;
    }

    public function testSell()
    {
        $order = [
            'type' => 'limit',
            'side' => 'sell',
            'product_id' => 'XLM-EUR',
            'price' => .5,
            'size' => 34
        ];

        $coinbase = new Client();
        $order = json_decode($coinbase->doRequest('/orders', $order, 'POST', true));

        $a=1;

    }

    public function testCancel()
    {
        $coinbase = new Client();
        return $coinbase->doRequest('/orders/' . '3e938e45-0a3a-40fb-b21d-356c38e57d5e', null, 'DELETE', true);
//        $order = json_decode($coinbase->doRequest('/orders', '3e938e45-0a3a-40fb-b21d-356c38e57d5e', 'DELETE', true));
    }

}

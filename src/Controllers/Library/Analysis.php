<?php

namespace Gula\Stockbroker\Controllers\Library;

use Gula\Stockbroker\Controllers\CoinbaseController;
use Gula\Stockbroker\Models\Coinbase\Products;
use Gula\Stockbroker\Models\Coinbase\ProductsHistoricalPrices;

class Analysis extends CoinbaseController
{
    protected $startDate;
    protected $endDate;
    protected float $increasePercentage = 0.05;

    public function start(string $startDate = null, string $endDate = null)
    {
        $this->startDate = $startDate;
        $this->endDate = $endDate;

//        $this->updateProductsPrices();
//        @todo weer activeren

        $mdlProcucts = new Products();

        foreach ($mdlProcucts->getProducts(false) as $product){
            $this->lowestPointReached($product->id, true);

            $a=1;
        }
    }

    private function lowestPointReached(string $idProduct, bool $sendMail = false)
    {
        $mdlProducts = new Products();
        $mdlHistory = new ProductsHistoricalPrices();

        $priceLow = $mdlHistory->getMin($idProduct, $this->startDate);
        $priceCurrent = $mdlProducts->getCurrentPrice($idProduct);

        if($priceLow !== 0.0){

            $increase = ($priceCurrent - $priceLow) / $priceLow;
            if($increase >= $this->increasePercentage && $increase <= (2*$this->increasePercentage))
            {
                $a=1;
            }
            if($sendMail === true){
                //sendmail or add to queue
            }
        }
    }

    protected function adviceSell(string  $idProduct, $sendMail = false)
    {

    }


}

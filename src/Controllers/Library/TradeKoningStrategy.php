<?php


namespace Gula\Stockbroker\Controllers\Library;


use Gula\Stockbroker\Controllers\CoinbaseController;
use Gula\Stockbroker\Models\Coinbase\Products;
use Gula\Stockbroker\Models\Coinbase\Strategies;
use test\Mockery\ArgumentObjectTypeHint;

class TradeKoningStrategy extends CoinbaseController
{
    protected $lookBackPeriod = '1 day';
    protected $depreciationPercentage = -10;
    protected $increasementPercentage = 10;
    protected $tradebleCoins = array('ETH-EUR');
    protected $stategyProducts = [];
    protected $strategyName = 'koning_strategy';


    public function analyze()
    {
        $this->stategyProducts = (new Strategies())->getTradebleProducts($this->strategyName);
        //sell
        //buy
        $this->placeOrders();

        echo 1;
    }


    protected function placeOrders()
    {
        $mdlProducts = new Products();
        $products = $mdlProducts->getProducts(false);

        $startDate = date("Y-m-d", strtotime('-1 days'));
        $endDate = date("Y-m-d", strtotime('+1 day'));


        foreach ($products as $product) {
            if (substr($product->id, -4) === '-EUR') {
                $rates = $this->getPricesSpecificPeriod($product->id, $startDate, $endDate);
                $priceCurrent = $rates[0][4];
                $price24Hrs = $rates[92][4];

                if (($priceCurrent - $price24Hrs) * 100 / $price24Hrs <= $this->depreciationPercentage) {

                    if ($this->isTradableProduct($product->id)) {

                        $a = 2;
                    }

                    //3.coin toegestaan én coin zit nog niet in portefeuille dan uitvoeren
                    //buy
                    echo $product->id . ': ' . (($priceCurrent - $price24Hrs) * 100 / $price24Hrs) . '<br>';
                    $a = 1;
                }
                //hier verder is goed


                $a = 1;
            }
        }

        $a = 1;
        //1/loopen door de munten
        //2.analyse
        //3.coin toegestaan én coin zit nog niet in portefeuille dan uitvoeren
        //anders mail sturen


        //niet voor munten die we al gebruiken

        //buyorder direct

        //sellorder
    }


    private function isTradableProduct(string $idProduct): bool
    {
        $tradable = false;

        foreach ($this->stategyProducts as $product) {
            if ($product->id_product === $idProduct) {
                $tradable = true;
                break;
            }
        }

        return $tradable;
    }

    protected function placeBuyOrder(array $params)
    {

    }

    protected function placeSellOrder(array $params)
    {

    }

    private function getIso8600Date($date)
    {
        $objDateTime = new DateTime($date);
    }
}

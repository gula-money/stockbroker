<?php


namespace Gula\Stockbroker\Controllers\Library;


use Gula\Stockbroker\Controllers\CoinbaseController;

/**
 * Class TradeBottomTakeProfitStrategy
 * @package Gula\Stockbroker\Controllers\Library
 * predict bottom -> buy await percentage profit -> sell
 */
class TradeBottomTakeProfitStrategy extends CoinbaseController
{
    protected $lookBackPeriod = '1 day';
    protected $depreciationPercentage = -10;
    protected $increasementPercentage = 10;
    protected $stategyProducts = [];
    protected $strategyName = 'bottom_take_top_strategy';
}

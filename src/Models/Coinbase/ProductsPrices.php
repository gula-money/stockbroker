<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Support\Facades\DB;

class ProductsPrices
{
    /**
     * @var string
     */
    protected $table = 'coinbase_products_prices';

    /**
     * @param string $idProduct
     * @param float $price
     */
    public function add(string $idProduct, float $price)
    {
        DB::table($this->table)
            ->insert(
                array(
                    'id_product' => $idProduct,
                    'price' => $price,
                )
            );
    }
}

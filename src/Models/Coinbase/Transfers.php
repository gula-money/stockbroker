<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Transfers extends Model
{

    protected $table = 'coinbase_transfers';

    public function getTransfers()
    {
        $this->updateTransfer();

        return DB::table($this->table)
            ->where('ignore', '=', false)
            ->orderBy('completed_at', 'DESC')
            ->get();
    }

    public function updateTransfer($data)
    {
        foreach ($data as &$transfer) {
            $transfer->details = json_encode($transfer->details);
            $transfer->created_at = date('Y-m-d H:i',strtotime($transfer->created_at));
            $transfer->completed_at = date('Y-m-d H:i',strtotime($transfer->completed_at));
            $transfer->processed_at = date('Y-m-d H:i',strtotime($transfer->processed_at));
            $transfer->canceled_at = date('Y-m-d H:i',strtotime($transfer->canceled_at));

            DB::table($this->table)
                ->updateOrInsert(array('id' => $transfer->id), (array)$transfer);
        }
    }

    /**
     * @return array
     */
    public function getTotals(): array
    {
        $withDraw = DB::table($this->table)
            ->where('type', '=', 'withdraw')
            ->where('ignore', '=', false)
            ->sum('amount');

        $deposit = DB::table($this->table)
            ->where('type', '=', 'deposit')
            ->where('ignore', '=', false)
            ->sum('amount');

        return ['deposit' => $deposit, 'withdraw' => $withDraw];
    }
}

<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Strategies extends Model
{
    protected $table = 'coinbase_strategies';

    public function getTradebleProducts(string $strategy)
    {
        return DB::table($this->table)
            ->where('id_strategy', '=', $strategy)
            ->get();
    }
}

<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Support\Facades\DB;

class Products
{
    /**
     * @var string
     */
    protected $table = 'coinbase_products';
    protected $model = 'products';

    /**
     * @param array $products
     */
    public function update(array $products)
    {
        foreach ($products as $product) {
            DB::table($this->table)->updateOrInsert(array('id' => $product->id), (array)$product);
        }

        $log = new Logs();
        $log->store($this->model, 'Products  updated');
    }

    /**
     * @param bool $activeCheck
     * @return \Illuminate\Support\Collection
     */
    public function getProducts(bool $activeCheck = false, $euroCheck = false)
    {
        $sql = DB::table($this->table)
            ->select(['id'])
            ->where('deleted', '=', false);

        if($activeCheck === true){
            $sql->where('active', '=', true);
        }

        if($euroCheck === true){
            $sql->where('id', 'like', '%-EUR');
        }

        return $sql->get();
    }

    /**
     * @param string $idProduct
     * @param float $value
     */
    public function updatePrice(string $idProduct, float $value)
    {
        DB::table($this->table)
            ->where('id', '=', $idProduct)
            ->update(['value' => $value]);
    }

    public function getCurrentPrice(string $idProduct): ?float
    {
        $result = DB::table($this->table)
            ->select(['value'])
            ->where('id','=',$idProduct)
            ->first();

        return (float) $result->value;
    }

}

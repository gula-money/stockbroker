<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Scenarios extends Model
{
    protected $table = 'coinbase_scenarios';
    protected $tableAccounts = 'coinbase_accounts';
    protected $table_products = 'coinbase_products';

    public function list()
    {
        return DB::table($this->table)
            ->where($this->table . '.deleted', '=', false)
            ->orderBy('id_order', 'ASC')
            ->orderBy('depending_on_scenario', 'ASC')
            ->orderBy('sort_order', 'DESC')
            ->orderBy('created_at', 'ASC')
            ->orderBy('executed_at', 'DESC')
            ->get();
    }

    public function getIdCreated()
    {
        return DB::table($this->table)
            ->insertGetId([]);
    }

    public function get($id)
    {
        return DB::table($this->table)
            ->where('id', '=', $id)
            ->first();
    }

    public function store($post)
    {
        unset($post['_token']);
        $post['deleted'] = key_exists('deleted', $post) && $post['deleted'] === 'on' ? 1 : 0;

        DB::table($this->table)
            ->where('id', '=', $post['id'])
            ->update($post);
    }

    public function disable($id)
    {
        DB::table($this->table)
            ->where('id', '=', $id)
            ->update(['deleted' => true]);

        $scenario = DB::table($this->table)->where('id', '=', $id)->first();

        if($scenario->id_order !== null){
            $order = json_decode($coinbase->doRequest('/orders', $scenario->id_order, 'DELETE', true));
        }
    }

    public function getOpen()
    {
        return DB::table($this->table)
            ->select($this->table . '.*', $this->table_products . '.value')
            ->leftJoin($this->table_products, $this->table_products . '.id', '=', $this->table . '.product_id')
            ->where('id_order', '=', null)
            ->where($this->table . '.deleted', '=', false)
            ->get();
    }

    public function updateScenario($data)
    {
        DB::table($this->table)
            ->where('id', '=', $data['id'])
            ->update($data);
    }
}

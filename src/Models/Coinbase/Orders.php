<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Support\Facades\DB;

class Orders
{
    /**
     * @var string
     */
    protected $table = 'coinbase_orders';
    protected $tableProducts = 'coinbase_products';
    protected $model = 'orders';

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getOrders()
    {
        $this->updateOrders();

        return DB::table($this->table)
            ->select(['coinbase_products.value', 'coinbase_orders.*',])
            ->leftJoin($this->tableProducts, 'coinbase_products.id', 'coinbase_orders.product_id')
            ->orderBy('done_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    /**
     *
     */
    public function updateOrders()
    {
        $coinbase = new Client();
        $log = new Logs();

        $orders = json_decode($coinbase->doRequest('/orders?status=all', null, 'GET', true));

        DB::table($this->table)->update(array('cancelled' => 1));

        foreach ($orders as $order) {
            $order->created_at = date('Y-m-d h:m:s', strtotime($order->created_at));
            if (!empty($order->done_at))
                $order->done_at = date('Y-m-d h:m:s', strtotime($order->done_at));
            $order->cancelled = 0;

            DB::table($this->table)->updateOrInsert(array('id' => $order->id), (array)$order);
        }

//        $log->store($this->model, 'Orders updated');
    }

    public function getAccountOrders($idProduct)
    {
        return DB::table($this->table)
            ->select(['coinbase_products.value', 'coinbase_orders.*'])
            ->leftJoin($this->tableProducts, 'coinbase_products.id', 'coinbase_orders.product_id')
            ->where('product_id', '=', $idProduct . '-EUR')
            ->where('cancelled', '=', false)
            ->where('created_at','>=', '2020-01-01')
            ->orderBy('status', 'DESC')
            ->orderBy('done_at', 'DESC')
            ->orderBy('created_at', 'DESC')
            ->get();

    }

    /**
     * @param string $idProduct
     * @return array
     */
    public function getTrading(string $idProduct): array
    {
        $idProduct = strtoupper($idProduct) . '-EUR';

        $bought = DB::table($this->table)
            ->where('status', '=', 'done')
            ->where('side', '=', 'buy')
            ->where('product_id', '=', $idProduct)
            ->where('done_at','>=', '2020-01-01')
            ->sum('executed_value');

        $sold = DB::table($this->table)
            ->where('status', '=', 'done')
            ->where('side', '=', 'sell')
            ->where('product_id', '=', $idProduct)
            ->where('done_at','>=', '2020-01-01')
            ->sum('executed_value');

        return ['bought' => $bought, 'sold' => $sold];
    }
}

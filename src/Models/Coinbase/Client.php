<?php

namespace Gula\Stockbroker\Models\Coinbase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Client extends Model
{
    protected $apiEndpoint;
    protected $key;
    protected $secret;
    protected $passphrase;

    public function __construct(array $attributes = [])
    {
        $this->setCredentials();
    }

    private function setCredentials()
    {
        $this->apiEndpoint = env('COINBASE_API_END_POINT');
        $this->key = env('COINBASE_KEY');
        $this->secret = env('COINBASE_SECRET');
        $this->passphrase = env('COINBASE_PASSPHRASE');
    }

    public function signature($request_path = '', $body = '', $timestamp = false,string $method = 'GET')
    {
        $body = is_array($body) ? json_encode($body) : $body;
        $timestamp = $timestamp ? $timestamp : time();

        $what = $timestamp . $method . $request_path . $body;

        return base64_encode(hash_hmac("sha256", $what, base64_decode($this->secret), true));
    }

    public function doRequest(string $path, array $post = null, $method, $log = false)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->apiEndpoint . $path);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLINFO_HEADER_OUT , 1);

        if($method == 'DELETE'){
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        }
        if ($method == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
        }

        $headers = array();
        $headers[] = "Content-Type: application/json";
        $headers[] = "CB-ACCESS-SIGN: " . $this->signature($path, ($post ? json_encode($post) :''), time(), $method);
        $headers[] = "CB-ACCESS-KEY: " . $this->key;
        $headers[] = "CB-ACCESS-PASSPHRASE: " . $this->passphrase;
        $headers[] = "CB-ACCESS-TIMESTAMP: " . time();

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)');

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);

        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result;
    }

    private static function logRequest(string $model, string $request, string $response)
    {
        DB::table('cb_log_requests')
            ->insert(array(
                'model' => $model,
                'request' => $request,
                'response' => $response,
            ));
    }

}

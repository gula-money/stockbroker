<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Support\Facades\DB;

class PaymentMethods
{
    /**
     * @var string
     */
    protected $table = 'coinbase_payment_methods';

    /**
     * @param array $methods
     */
    public function update(array $methods)
    {
        foreach ($methods as $method) {
            DB::table($this->table)->updateOrInsert(array('id' => $method->id), (array)$method);
        }
    }

}

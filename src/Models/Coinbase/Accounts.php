<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Support\Facades\DB;

class Accounts
{
    /**
     * @var string
     */
    protected $table = 'coinbase_accounts';
    protected $table_products = 'coinbase_products';

    /**
     * @param array $accounts
     */
    public function update(array $accounts)
    {
        foreach ($accounts as $account) {
            DB::table($this->table)->updateOrInsert(array('id' => $account->id), (array)$account);
        }
    }

    /**
     * @param bool $ignoreInactive
     * @return \Illuminate\Support\Collection
     */
    public function getAccounts(bool $getEfficiency = false)
    {
        $accounts = DB::table($this->table)
            ->select(['coinbase_accounts.*', 'coinbase_products.value'])
            ->join($this->table_products, $this->table_products . '.id', '=', DB::raw('CONCAT (coinbase_accounts.currency, "-EUR")') )
            ->where($this->table_products . '.deleted', '=', false)
            ->orderBy('currency', 'ASC')
            ->orderBy(DB::raw('balance * value'), 'DESC')
            ->get();

        return ($getEfficiency ? $this->fillEfficiency($accounts) : $accounts);
    }

    /**
     * @return float
     */
    public function getPortfolioValue():float
    {
        $coinValue = DB::table($this->table)
            ->join($this->table_products, $this->table_products . '.id', '=', DB::raw('CONCAT (coinbase_accounts.currency, "-EUR")') )
            ->sum(DB::raw('balance * value'));

        $euroValue = DB::table($this->table)
            ->where('currency','=', 'EUR')
            ->sum('balance');

        return $coinValue + $euroValue;
    }

    /**
     * @param string $idProduct
     * @return float
     */
    public function getAccountValue(string $idProduct): float
    {
        return DB::table($this->table)
            ->join($this->table_products, $this->table_products . '.id', '=', DB::raw('CONCAT (coinbase_accounts.currency, "-EUR")') )
            ->where($this->table_products . '.id', '=', $idProduct . '-EUR')
            ->sum(DB::raw('balance * value'));
    }

    public function fillEfficiency($accounts)
    {
        $mdlOrders = new Orders();

        foreach ($accounts as &$account){
            $orderSums = $mdlOrders->getTrading($account->currency);
            $account->bought = $orderSums['bought'];
            $account->sold = $orderSums['sold'];
        }

        return $accounts;
    }

    public function getAccount($idProduct)
    {
        return DB::table($this->table)
            ->leftJoin($this->table_products, 'coinbase_products.base_currency', '=', 'currency')
            ->where('currency', '=', $idProduct)
            ->first();
    }

}

<?php


namespace Gula\Stockbroker\Models\Coinbase;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Logs extends Model
{
    protected $table = 'coinbase_logs';

    public function store(string $model, $entry)
    {
        DB::table($this->table)
            ->insert(['model' => $model, 'entry' => $entry]);
    }
}

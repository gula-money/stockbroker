<?php


namespace Gula\Stockbroker\Models\Coinbase;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class StrategyQueue extends Model
{
    protected $table = 'coinbase_strategy_queue';
    protected $tableAccounts = 'coinbase_accounts';
    protected $table_products = 'coinbase_products';

    public function store($post)
    {
        DB::table($this->table)->insert($post);
    }
}

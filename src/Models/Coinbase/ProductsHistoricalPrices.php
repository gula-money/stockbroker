<?php


namespace Gula\Stockbroker\Models\Coinbase;


use Illuminate\Support\Facades\DB;

class ProductsHistoricalPrices
{
    /**
     * @var string
     */
    protected $table = 'coinbase_products_historical_prices';

    /**
     * @param string $product
     * @param string $monday
     * @return bool
     */
    public function historyExists(string $product, string $monday): bool
    {
        $result = DB::table($this->table)
            ->where('id_product', '=', $product)
            ->where('monday', '=', $monday)
            ->first();

        return $result ? true : false;
    }

    /**
     * @param string $idProduct
     * @param array $rates
     * @param string $monday
     */
    public function updateRates(string $idProduct, array $rates, string $monday)
    {
        foreach ($rates as $rate) {
            DB::table($this->table)
                ->updateOrInsert(
                    array(
                        'id_product' => $idProduct,
                        'time' => $rate[0]
                    ),
                    array(
                        'timestamp' => $rate[0],
                        'time' => date('Y-m-d H:i',$rate[0]),
                        'low' => $rate[1],
                        'high' => $rate[2],
                        'open' => $rate[3],
                        'close' => $rate[4],
                        'volume' => $rate[5],
                        'monday' => $monday
                    )
                );
            $a=1;
        }
    }

    public function getMin(string $idProduct, string $startDate)
    {
        $result = DB::table($this->table)
            ->select(DB::raw('MIN(low) as min'))
            ->where('id_product', '=', $idProduct)
            ->where('time', '>=', $startDate)
            ->first();

        return (float) $result->min;
    }
}

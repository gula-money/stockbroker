<p align="center"><img src="https://cms.gula.nl/resizer/300x100/gula.nl/logo/gula_logo.png"></p>
<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About stockbroker
Handle different brokers through api


## After installation
- php artisan vendor:publish --tag=public --force

git remote set-url origin git@gitlab.com:gula-cms/stockbroker.git 
